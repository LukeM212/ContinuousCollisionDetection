const canvas = document.getElementById('canvas'),
	ctx = canvas.getContext('2d');

ctx.lineWidth = 2;

let px = 0, py = 0,
	vx = 0, vy = 0,
	mx = 0, my = 0,
	r = 10;

let block = [
	{ x: 200, y: 200 },
	{ x: 190, y: 200 },
	{ x: 190, y: 250 },
	{ x: 230, y: 250 },
	{ x: 230, y: 200 },
	{ x: 220, y: 200 },
	{ x: 220, y: 240 },
	{ x: 200, y: 240 }
];

document.onmousemove = e => {
	mx = (e.clientX - canvas.offsetLeft);
	my = (e.clientY - canvas.offsetTop);
};

function draw() {
	ctx.clearRect(0, 0, 400, 400);
	
	ctx.beginPath();
	ctx.moveTo(block[block.length - 1].x, block[block.length - 1].y);
	for (const p of block) ctx.lineTo(p.x, p.y);
	ctx.stroke();
	
	ctx.beginPath();
	ctx.arc(px, py, r, 0, 2 * Math.PI);
	ctx.stroke();
}

function tick() {
	vx = vx * 0.9 + (mx - px) * 0.1;
	vy = vy * 0.9 + (my - py) * 0.1;
	
	let maxDist = 1;
	let dist = 1;
	let finished = false;
	while (!finished && maxDist > 0) {
		if (!(dist > 0)) finished = true;
		
		dist = maxDist;
		
		let line = null;
		let last = block[block.length - 1];
		for (const p of block) {
			let lx = p.x - last.x,
				ly = p.y - last.y;
			
			let det = vx * ly - vy * lx;
			if (det > 0) {
				let l = Math.sqrt(lx * lx + ly * ly);
				
				let e1 = p.x - r * ly / l - px,
					e2 = p.y + r * lx / l - py;
				
				let p1 = (ly * e1 - lx * e2) / det,
					p2 = (vx * e2 - vy * e1) / det;
				
				if (p2 > 0 && p2 < 1 && p1 > -1 && p1 < dist) {
					dist = p1;
					line = { x: lx, y: ly };
				}
			}
			
			let a = vx * vx + vy * vy,
				b = 2 * (vx * (px - p.x) + vy * (py - p.y)),
				c = (px - p.x) * (px - p.x) + (py - p.y) * (py - p.y) - r * r;
			
			let d = b * b - 4 * a * c;
			if (d > 0.0000001) {
				let newDist = (-b - Math.sqrt(d)) / (2 * a);
				if (newDist > -1 && newDist < dist) {
					dist = newDist;
					line = { x: py + vy * dist - p.y, y: p.x - px - vx * dist };
				}
			}
			
			last = p;
		}
		
		px += vx * dist;
		py += vy * dist;
		
		if (line) {
			let m = (vx * line.x + vy * line.y) / (line.x * line.x + line.y * line.y)
			vx = line.x * m;
			vy = line.y * m;
		}
		
		maxDist -= dist;
		
		if (dist > 0) finished = false;
	};
}

function loop() {
	draw();
	tick();
	
	window.requestAnimationFrame(loop);
}

window.requestAnimationFrame(loop);
